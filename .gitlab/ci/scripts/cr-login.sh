#!/usr/bin/env bash

CI_SERVER_HOST="${CI_SERVER_HOST:-gitlab.com}"
CI_SERVER_PORT="${CI_SERVER_PORT:-443}"
CI_REGISTRY="${CI_REGISTRY:-"registry.$CI_SERVER_HOST"}"

# GitLab Container Registry on GitLab SaaS
echo "==> Logging in to GitLab Container Registry/Proxy..."
if [[ $GITLAB_CONTAINER_REGISTRY_PASSWORD != "" ]] && [[ $GITLAB_CONTAINER_REGISTRY_USERNAME != "" ]]; then
  docker login "$CI_REGISTRY" --username "$GITLAB_CONTAINER_REGISTRY_USERNAME" --password "$GITLAB_CONTAINER_REGISTRY_PASSWORD"
  docker login "$CI_SERVER_HOST:$CI_SERVER_PORT" --username "$GITLAB_CONTAINER_REGISTRY_USERNAME" --password "$GITLAB_CONTAINER_REGISTRY_PASSWORD"
else
  echo "warning: No GitLab Deploy Token to use for Dependency Proxy and GitLab Container Registry, skipping..."
fi

echo "===> Logging in to RHQCR...."
if [[ $RHQCR_SERVICE_ACCOUNT_PASSWORD != "" ]] && [[ $RHQCR_SERVICE_ACCOUNT_USERNAME != "" ]]; then
  docker login "quay.io" --username "$RHQCR_SERVICE_ACCOUNT_USERNAME" --password "$RHQCR_SERVICE_ACCOUNT_PASSWORD"
else
  echo "warning: No RHQCR login info!"
fi