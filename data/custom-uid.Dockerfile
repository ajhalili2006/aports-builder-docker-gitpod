ARG BASE=quay.io/ajhalili2006/bulldozer-alpine-gp:edge
FROM ${BASE}

ARG UID=1000
ARG USERNAME=ajhalili2006
RUN doas adduser -u "$UID" -D -h "/home/bulldozer" "$USERNAME" \
    && doas adduser "${USERNAME}" "abuild" \
    && doas adduser "${USERNAME}" "bulldozer" \
    && doas adduser "${USERNAME}" wheel

USER ${USERNAME}

# Customize your devenv here below, like installing vim/emacs or even install tools to ~/.local/bin
# RUN doas apk add vim
# ENV EDITOR=vim