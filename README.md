# Aports Builder Docker image in Gitpod

This repository was originally from Alpine Linux GitLab instance and is being mirrored to GitLab SaaS for CI builds with BuildKit enabled to support multi-arch builds.

> :warning: **DO NOT REUSE THE APK SIGNING KEYS COMMITED HERE!** These keys are for development purposes only and should not be used in production. If you ever use them, your packages are in danger.

## Caveats

* Since Gitpod uses non-Busybox version of `adduser` when adding the gitpod user to the workspace image for compartibiliy, that ma
* ~~Setting up an chroot might helps, but this might be tricker because of nature of multi-tenant setup.~~ Theoretically, this might work, but since files under `/workspace` are isolated from each other, results may vary.
* If you cannot edit the `$HOME` directory inside the container, try `doas chown -Rv bulldozer:bulldozer /home/bulldozer` first on the container shell.

## Usage

Make sure to set `APORTS_REPO_FORK_URL` variable to the Git HTTPS URL of your aports fork, otherwise it will fallback to the default one.
You may needed to connect your Alpine Linux GitLab account to Gitpod.io [through this link][oauth] to ensure that you are being authenicated and can clone from/push to repositories smoothly

[oauth]: https://gitlab.alpinelinux.org/oauth/authorize?response_type=code&redirect_uri=https%3A%2F%2Fgitpod.io%2Fauth%2Fgitlab.alpinelinux.org%2Fcallback&scope=api%20read_user%20read_repository&client_id=acc4930d777520445689ac17f9387836d793c47bb02ee910491826d587d9f98d

When opened from Gitpod, the Alpine Linux bulldozer image will build from source behind the scenes as part of the prebuilds. Once successfully create an workspace, you'll be dropped to an Alpine Linux shell inside an Docker container.
An additional terminal is there for performance observibility through the `htop` command.

### On local machine / VPS

Since these images are built for use in Gitpod workspaces, you may need to use the `./scripts/build-localdev` to
fix permission errors as much as possible.

```sh
# Usage: ./scripts/build-localdev $UID $USERNAME [IMAGE TAG]
./scripts/build-localdev $(id -r) $(id -un) quay.io/ajhalili2006/bulldozer-alpine-gp:edge
# ===> Build information for debugging purposes
#      ID: $ID
#      Name: $USER
#      Base image: quay.io/ajhalili2006/bulldozer-alpine-gp:edge
# ====> Build logs goes below this line, starts in 3 seconds...
# ...
# ...
# info: Build successful. Note that to avoid permission issues, use the following
# info: parameters when running the run-alpine-devenv script.
# info:
# info:  ./scriptsw/run-alpine-devenv docker.alpinelinux.org/ajhalili2006/buildscraft:alpine-gpws-<some-uuid-string-here>
```

Scroll down to the **Build from source** section if you want to reproduce the image locally.

## Available Commands

Scripts are literally copied from the sources for the Docker images Alpine Linux devs use for the CI stuff on merge requests, along with an addon.

* `apkbuild-shellcheck` - Shortcut for adding `-s ash e SC2016 -e SC2086 e SC2169 e SC2155 -e SC2100 -e SC2209 -e SC2030 -e SC2031 -e SC1090 -xa /usr/share/abuild/APKBUILD_SHIM` as flags for `shellcheck`.
* `build-packages` - Script to build packages without `doas apk update; cd <category/<package>; abuild -r`
* `changed-aports` - List all changed APKBUILD files and compare it against upstream
* `lint` - script to run the lint CI, simulating the GitLab CI lint step for PRs.
* `workspace-init` - handles the `apk update` step and execs to the command you wanted.
  * There's an intentional bug when you do `docker run` manually the way `run-alpine-devenv` script does, along with `ash -l <command goes here>`, you may be dropped to the interactive shell instead. Will be fixed later.

## Build from source

1. Clone the repo, either from GitLab SaaS or Alpine Linux's GitLab instance.

```sh
# Alpine Linux GitLab mirror - you may prompted for your PAT since it's internal by default
git clone https://gitlab.alpinelinux.org/ajhalili2006/aports-builder-docker-gitpod.git ~/bulldozer-gp

# GitLab SaaS - recommended
git clone https://gitlab.com/ajhalili2006/aports-builder-docker-gitpod.git ~/bulldozer-gp
```

2. Build using th default build arguments, excluding the user ID if you want to run it locally:

```sh
# Usage: ./scripts/build [ALPINE_RELEASE] [UID] [GID] [TAG]
./scripts/build 3.14 1000 quay.io/ajhalili2006/bulldozer-alpine-gp:edge
# ====> Build info for technical support:
#       Alpine release: 3.14
#       Bulldozer UID: 1000
#       Build tag: quay.io/ajhalili2006/bulldozer-alpine-gp:edge
# ====> Build logs goes below this line, starts in 10 seconds...
# ...
# ...
# ...
# info: Build successful. Use the tag quay.io/ajhalili2006/bulldozer-alpine-gp:edge
# info: when using the bundled ./scripts/run-alpine-devenv script.
# info:
# info: ./scripts/run-alpine-devenv quay.io/ajhalili2006/bulldozer-alpine-gp:edge
```

3. Test if your build work:

```sh
# clone the aports tree
git clone https://gitlab.alpinelinux.org/alpine/aports "$PWD/src"

./scripts/run-alpine-devenv [TAG] # if you `direnv allow`, just go run-alpine-devenv [TAG]
```

